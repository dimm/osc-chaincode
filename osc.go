package main

import (
	"encoding/json"
	"fmt"
	//"github.com/rs/xid"
	"encoding/base64"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	"github.com/hyperledger/fabric/protos/peer"
	"github.com/hyperledger/fabric/core/chaincode/shim/ext/cid"
)


type OSCAsset struct {
}

func (t *OSCAsset) Init(stub shim.ChaincodeStubInterface) peer.Response {
	return shim.Success(nil)
}

func (t *OSCAsset) Invoke(stub shim.ChaincodeStubInterface) peer.Response {
	fn, args := stub.GetFunctionAndParameters()
	var result string
	var err error
	if fn == "set" {
		result, err = set(stub, args)
	} else {
		result, err = get(stub, args)
	}
	if err != nil {
		return shim.Error(err.Error())
	}

	return shim.Success([]byte(result))
}

func set(stub shim.ChaincodeStubInterface, args []string) (string, error) {
	if len(args) != 2 {
		return "", fmt.Errorf("Incorrect arguments. Expecting pub json ")
	}

	switch args[0] {
	case "addAsset":
		var pub Publication
		if err := json.Unmarshal([]byte(args[1]), &pub); err != nil {
			return "Error", fmt.Errorf("Failed to unmarshar pub: %s", err.Error())
		}

		//https://github.com/hyperledger/fabric/blob/master/core/chaincode/shim/ext/cid/README.md
		creator, err := cid.GetID(stub) // user id unique to MSP - long string

		//creator, err := stub.GetCreator() // public cert

		if err != nil {
			return "Error", fmt.Errorf("Failed to get creator: %s", err.Error())
		}

		pub.CreatorID = fmt.Sprintf("%s", creator)

		pubb, err := json.Marshal(&pub)
		if err != nil {
			return "Error", fmt.Errorf("Failed to marshar pub: %s", err.Error())
		}

		id := New() // Unique publication ID

		if err := stub.PutState(id.String(), pubb); err != nil {
			return "Error", fmt.Errorf("Failed to set pub: %s", args[0])
		}

		indexName := "userid~pubname"
		userPubIndexKey, err := stub.CreateCompositeKey(indexName, []string{pub.UserID, pub.Name})
		if err != nil {
			return "Error", fmt.Errorf("Failed to gen asset index: %s", err.Error())
		}

		if err := stub.PutState(userPubIndexKey, []byte{0x00}); err != nil {
			return "Error", fmt.Errorf("Failed to gen asset index: %s", err.Error())
		}

		return get(stub, []string{"query"})
	}
	return "No function", nil
}

// Get returns the value of the specified asset key
func get(stub shim.ChaincodeStubInterface, args []string) (string, error) {
	if len(args) != 1 {
		return "", fmt.Errorf("Incorrect arguments.")
	}

	creator, err := cid.GetID(stub) // user id unique to MSP - long string
	if err != nil {
		return "", fmt.Errorf("Error getting creator %s", err.Error())
	}

	queryString := fmt.Sprintf("{\"selector\":{\"creatorid\":\"%s\"}}", creator)
	resultsIterator, err := stub.GetQueryResult(queryString)
	if err != nil {
		return "", err
	}
	defer resultsIterator.Close()


	pubs := []UserPublication{}

	for resultsIterator.HasNext() {
		queryResponse, _ := resultsIterator.Next()
		pub := UserPublication{
			Key: queryResponse.Key,
			Data: base64.StdEncoding.EncodeToString(queryResponse.Value),
		}
		pubs = append(pubs, pub)
	}

	if b, err := json.Marshal(pubs); err != nil {
		return "Error", fmt.Errorf("Failed to marshar pubs: %s", err.Error())
	} else {
		return fmt.Sprintf("%s", b), nil
	}
}


// main function starts up the chaincode in the container during instantiate
func main() {
	if err := shim.Start(new(OSCAsset)); err != nil {
		fmt.Printf("Error starting OSCAsset chaincode: %s", err)
	}
}